import * as _ from 'lodash';

function CountyData(props) {
  const data = _.zipObject(_.map(props.data, x => x.CandidateName), props.data);
  const winningParty = _.chain(props.data)
    .sortBy(x => -parseFloat(x.Votes))
    .head()
    .value().PartyName;
  return (
    <tr>
      <td className={winningParty}>{props.countyName}</td>
      {props.candidates.map(candidate => (
        <>
        <td className={data[candidate].PartyName}>{data[candidate].ProvisionalVotes} {data[candidate].partyName}</td>
        <td className={data[candidate].PartyName}>{data[candidate].MailInVotes}</td>
        <td className={data[candidate].PartyName}>{data[candidate].ElectionDayVotes}</td>
        <td className={data[candidate].PartyName}>{parseFloat(data[candidate].ProvisionalVotes) +
             parseFloat(data[candidate].MailInVotes) +
             parseFloat(data[candidate].ElectionDayVotes)}</td>
        </>
      ))}
    </tr>
  )
}

function Counties(props) {
  return (
    <>
      <table>
        <thead>
          <tr>
            <td></td>
            {props.candidates.map(candidate => (
              <>
              <td colspan="4">{candidate}</td>
              </>
            ))}
          </tr>
          <tr>
            <td>County</td>
            {props.candidates.map(candidate => (
              <>
              <td>Prov</td>
              <td>Mail</td>
              <td>Day</td>
              <td>Total</td>
              </>
            ))}
          </tr>
        </thead>
        {_.toPairs(props.counties).map(county => (<CountyData key={county[0]} countyName={county[0]} candidates={props.candidates} data={county[1]} />))}
      </table>
    </>
  );
}

function Returns(props) {
  const fieldsToSum = ['MailInVotes', 'ElectionDayVotes', 'ProvisionalVotes'];
  const counties = props.data.Election.Statewide[0];
  const candidates = _.chain(counties).values().head().map(x => x.CandidateName).value();
  const totals = {};
  _.forEach(candidates, candidate => {
    totals[candidate] = {};
    _.forEach(fieldsToSum, field => {
      totals[candidate][field] = _.chain(counties)
        .valuesIn()
        .flatten()
        .filter(x => x.CandidateName === candidate)
        .map(x => parseFloat(x.[field]))
        .sum(field)
        .value();
    });
  });
  const sortedCandidates = _.sortBy(candidates, candidate => -_.chain(totals[candidate]).values().sum().value());
  const totalVotes = _.chain(totals)
    .valuesIn()
    .map(x => _.chain(x).values().sum().value())
    .sum()
    .value();
  const partys = _.zipObject(
    _.chain(counties).values().head().map(x => x.CandidateName).value(),
    _.chain(counties).values().head().map(x => x.PartyName).value()
  );
  return (
    <>
    <h2>{props.name}</h2>
    <table>
      <thead>
        <tr>
          <td>Choice</td>
          <td>Provisional</td>
          <td>Mail-In</td>
          <td>Election Day</td>
          <td>Total</td>
          <td>%</td>
        </tr>
      </thead>
      {sortedCandidates.map(candidate => (
        <tr className={partys[candidate]} key={candidate}>
          <td>{candidate}</td>
          <td>{totals[candidate].ProvisionalVotes}</td>
          <td>{totals[candidate].MailInVotes}</td>
          <td>{totals[candidate].ElectionDayVotes}</td>
          <td>{_.chain(totals[candidate]).values().sum().value()}</td>
          <td>{Math.round(_.chain(totals[candidate]).values().sum().value() / totalVotes * 10000) / 100}</td>
        </tr>
      ))}
    </table>
    <Counties candidates={sortedCandidates} counties={counties} />
    </>
  );
}

export default Returns;
