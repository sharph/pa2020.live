#!/usr/bin/python3.7

import requests
import json
import time
import os
import shutil

TS_URL = 'https://electionreturns.pa.gov/api/ElectionReturn/GetUpdatedTimeStamp?methodName=LastUpdatedTimeStamp'

ELECTION_URLS = {
    'President of the United States':
    'https://electionreturns.pa.gov/api/ElectionReturn/GetCountyBreak?officeId=1&districtId=1&methodName=GetCountyBreak&electionid=undefined&electiontype=undefined&isactive=undefined',
    'Attorney General':
    'https://electionreturns.pa.gov/api/ElectionReturn/GetCountyBreak?officeId=5&districtId=1&methodName=GetCountyBreak&electionid=undefined&electiontype=undefined&isactive=undefined',
    'Auditor General':
    'https://electionreturns.pa.gov/api/ElectionReturn/GetCountyBreak?officeId=6&districtId=1&methodName=GetCountyBreak&electionid=undefined&electiontype=undefined&isactive=undefined',
    'State Treasurer':
    'https://electionreturns.pa.gov/api/ElectionReturn/GetCountyBreak?officeId=7&districtId=1&methodName=GetCountyBreak&electionid=undefined&electiontype=undefined&isactive=undefined'
}

MODE = 'live'
SLEEP_TIME = int(os.getenv('SLEEP_TIME', 60))
DEST_FILE = os.getenv('DEST_FILE', '/tmp/test.json')
TS_FILE = os.getenv('TS_FILE', '/tmp/testts.json')


def main():
    last_time = None
    last_data = None
    while True:
        ts = requests.get(TS_URL).json()
        print('ts:', ts)
        if last_time != ts:
            last_time = ts
        else:
            continue
        out = {
            'elections': {},
            'mode': MODE
        }
        for name, url in ELECTION_URLS.items():
            print(name)
            out['elections'][name] = json.loads(requests.get(url).json())
            print('  ok')

        if last_data != out:
            last_data = out
        else:
            print('data is the same')
            time.sleep(SLEEP_TIME)
            continue

        if os.path.exists('/tmp/tmpfile.json'):
            os.unlink('/tmp/tmpfile.json')
        with open('/tmp/tmpfile.json', 'w') as f:
            f.write(json.dumps(out))
        if os.path.exists(DEST_FILE):
            os.unlink(DEST_FILE)
        shutil.copy('/tmp/tmpfile.json', DEST_FILE)
        print(DEST_FILE, 'written')
        if os.path.exists(TS_FILE):
            os.unlink(TS_FILE)
        with open(TS_FILE, 'w') as f:
            f.write(json.dumps(last_time))
        print(TS_FILE, 'written')

        time.sleep(SLEEP_TIME)


if __name__ == '__main__':
    main()
