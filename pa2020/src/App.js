import React, { Component } from 'react';

import './App.css';
import Returns from './components/Returns';

const ELECTIONS = ['President of the United States',
                   'Attorney General',
                   'Auditor General',
                   'State Treasurer'];

const TS_URL = '/data/timestamp.json';
const DATA_URL = '/data/electionData.json';

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {ts: null, mode: 'loading', elections: false, countdown: 10, error: false};
  }

  tick() {
    this.setState({countdown: this.state.countdown - 1});
    if(this.state.countdown === 0) {
      this.update();
    }
  }

  update() {
    if (document.visibilityState !== "visible") {
      this.setState({countdown: 10, error: true});
      return;
    }
    fetch(TS_URL + '?' + getRandomInt(100000))
      .then(response => response.json())
      .then(data => {
        if (this.state.ts !== data) {
          this.fetchNewData(data);
        } else {
          this.setState({countdown: 10, error: false});
        }
      })
      .catch(() => {
        this.setState({countdown: 30, error: true});
      });
  }

  fetchNewData(ts) {
    fetch(DATA_URL + '?' + getRandomInt(100000))
      .then(response => response.json())
      .then(data => {
        this.setState({ts: ts, mode: data.mode || '??', elections: data.elections, error: false});
      })
      .catch(() => this.setState({error: true}))
      .finally(() => this.setState({countdown: 10}));
  }

  componentDidMount() {
    this.updateTimer = setInterval(() => this.tick(), 1000);
    this.update();
  }

  componentWillUnmount() {
    clearInterval(this.updateTimer);
  }

  render() {
    return (
      <div className="App">
        <h1>pa2020.<span className={'mode-' + this.state.mode}>{this.state.mode}</span><span className={this.state.error ? 'error' : 'light' }>{this.state.countdown}</span></h1>
        <h1>{this.state.ts}</h1>
        {this.state.elections ?
          ELECTIONS.map(name => (<Returns data={this.state.elections[name]} name={name} key={name} />))
        : <></>}
      </div>
    );
  }
}

export default App;
